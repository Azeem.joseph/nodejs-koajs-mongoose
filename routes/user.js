const router = require('@koa/router')();
const Koa = require('koa');
const userController = require('../controller/user');

const app = new Koa();


router.get('/getUsers',userController.getUser);

router.post('/insertUser',userController.createUser);


app
  .use(router.routes())
  .use(router.allowedMethods());


module.exports = router;

