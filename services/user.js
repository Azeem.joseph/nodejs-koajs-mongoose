const User = require("../models/user");

exports.getUserFromDb = (username) => {
  console.log(
    "getUserFromDb called ctx.request.body :",
    username
  );
  const data = User.findOne({ username: username});
  return data;
};

exports.createUserInDb = async (user) => {
  var newUser = new User(user);

  newUser.save();
  return user;
};
