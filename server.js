const Koa = require("koa");
const router = require("@koa/router")();
const mongoose = require("mongoose");
const bodyParser = require("koa-bodyparser");
const cors = require('@koa/cors');
const helmet = require('koa-helmet');
const connecTionString = require("./utils/connectionString");


const PORT = 3000;

const app = new Koa();
app.use(cors());
app.use(helmet());
app.use(bodyParser());

const user = require('./routes/user');


router.use(user.routes());


app.use(router.routes()).use(router.allowedMethods());

mongoose
  .connect(connecTionString, {useNewUrlParser: true, useUnifiedTopology: true})
  .then((result) => {
    app.listen(PORT, () => {
        console.log("Connected with Atlas (MongDB) and Koa server is listening on PORT : ", PORT);
    });
  })
  .catch((err) => {
    console.log(err);
  });

  
