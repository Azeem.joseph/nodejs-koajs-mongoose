const userDbService = require("../services/user");

exports.getUser = async (ctx) => {
  let user = ctx.request.querystring;
  const UsersList = await userDbService.getUserFromDb(user);
  ctx.body = { message: "All users in MongoDB ", userslist: UsersList };
};

exports.createUser = async (ctx) => {
  let userData = ctx.request.body;
  const InsertedUser = await userDbService.createUserInDb(userData);
  ctx.body = { message: "User inserted successfully", user: InsertedUser };
};
